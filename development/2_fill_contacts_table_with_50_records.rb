names = File.open(File.dirname(__FILE__) + '/fake_contacts_names.txt').readlines
unless names.length == Contact.all.size
  names.map(&:chomp).map do |name|
    Contact.create(name: name)
  end
end

puts "DEBUG : #{Contact.count} Contact records in the db."

__END__
neguses
simile
pyrrhonist
prefertile
intertwined
autoharp
schaffhausen
seismoscope
iapyx
papillose
predecease
amoeba
nonbristled
brioches
develop
carangoid
benempted
dollarfish
preincreased
overcharge
bartizaned
recently
vegetate
cereous
tamale
struttingly
transactor
cowardice
clitoris
craniums
bronchitic
panmunjom
goosefishes
multirole
escheator
binocle
encrinite
tubular
unriddler
branchia
disastrous
actinon
antimasque
unimplied
repoussage
wishfulness
couperin
untonsured
sulfonal
kenotic
