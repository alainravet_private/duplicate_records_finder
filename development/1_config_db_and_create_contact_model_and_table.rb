require 'rubygems'
require 'bundler/setup'

require 'data_mapper'
DataMapper.setup(:default, "sqlite://#{File.dirname(__FILE__)}/test.db")

class Contact
  include DataMapper::Resource
  property :id,         Serial
  property :name,       String
end

DataMapper.auto_upgrade!
