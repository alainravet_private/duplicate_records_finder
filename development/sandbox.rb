require_relative '1_config_db_and_create_contact_model_and_table'
require_relative '2_fill_contacts_table_with_50_records'

require 'levenshtein'


records = Contact.all
permutations = []
records.each do |rec_a|
  records.each do |rec_b|
     unless rec_b.id <= rec_a.id
       distance = Levenshtein.distance(rec_a.name, rec_b.name)
       permutations << [rec_a, rec_b, distance]
     end
  end
end

p = permutations.sort_by(&:last).group_by(&:last)
p.keys.take(1).each do |key|
  puts "Key: #{key}"
  puts p[key].inspect
end

puts Levenshtein.distance('raavez', 'gravet')
