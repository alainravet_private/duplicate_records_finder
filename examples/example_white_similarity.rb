# step 0 : create a Contact model and fill the DB with 50 Contact records
  require_relative '../development/1_config_db_and_create_contact_model_and_table'
  require_relative '../development/2_fill_contacts_table_with_50_records'
  # class Contact
  #   include DataMapper::Resource
  #   property :id,         Serial
  #   property :name,       String
  # end

####################################################################################
#                           HOW TO USE IN YOUR PROJECT :                           #
####################################################################################

# step 1 :
#---------

  require_relative '../lib/white_similarity_duplicates_finder'

# step 2 :
#---------

Contact.extend WhiteSimilarityDuplicatesFinder

# step 3 :
#---------

min_similarity = 0.5
pairs_of_duplicates = Contact.find_duplicates_with_white_similarity_greater_or_equal_to(:name, min_similarity)

puts pairs_of_duplicates.first.inspect
puts pairs_of_duplicates. last.inspect

  # => [0.75, #<Contact @id=1 @name="christion">, #<Contact @id=2 @name="christian">]
  # => [0.5, #<Contact @id=31 @name="bronchitic">, #<Contact @id=40 @name="branchia">]

# step 4 :
#---------

pairs_of_duplicates.group_by(&:first).each_pair do |similarity, pairs_of_duplicates|
  puts "\nSimilarity: #{similarity}"
  puts "---------------"
  puts pairs_of_duplicates.inspect
end


  # Similarity: 0.75
  # ---------------
  # [[0.75, #<Contact @id=1 @name="christion">, #<Contact @id=2 @name="christian">]]
  #
  # Similarity: 0.6
  # ---------------
  # [[0.6, #<Contact @id=11 @name="predecease">, #<Contact @id=19 @name="preincreased">]]
  #
  # Similarity: 0.5
  # ---------------
  # [[0.5, #<Contact @id=31 @name="bronchitic">, #<Contact @id=40 @name="branchia">]]
