# step 0 : create a Contact model and fill the DB with 50 Contact records
  require_relative '../development/1_config_db_and_create_contact_model_and_table'
  require_relative '../development/2_fill_contacts_table_with_50_records'
  # class Contact
  #   include DataMapper::Resource
  #   property :id,         Serial
  #   property :name,       String
  # end

####################################################################################
#                           HOW TO USE IN YOUR PROJECT :                           #
####################################################################################

# step 1 :
#---------

  require_relative '../lib/levenshtein_duplicates_finder'

# step 2 :
#---------

Contact.extend LevenshteinDuplicatesFinder

# step 3 :
#---------

max_distance = 4
pairs_of_duplicates = Contact.find_duplicates_with_levenshtein_distance_smaller_or_equal_to(:name, max_distance)

puts pairs_of_duplicates.first.inspect
puts pairs_of_duplicates. last.inspect

  # => [3, #<Contact @id=2 @name="simile">, #<Contact @id=25 @name="tamale">]
  # => [4, #<Contact @id=31 @name="bronchitic">, #<Contact @id=40 @name="branchia">]

# step 4 :
#---------

pairs_of_duplicates.group_by(&:first).each_pair do |distance, pairs_of_duplicates|
  puts "\nDistance : #{distance}"
  puts "---------------"
  puts pairs_of_duplicates.inspect
end

  # Distance : 3
  # ---------------
  # [[3, #<Contact @id=2 @name="simile">, #<Contact @id=25 @name="tamale">]]

  # Distance : 4
  # ---------------
  # [[4, #<Contact @id=2 @name="simile">, #<Contact @id=36 @name="binocle">],
  #  [4, #<Contact @id=11 @name="predecease">, #<Contact @id=19 @name="preincreased">],
  #  [4, #<Contact @id=14 @name="brioches">, #<Contact @id=36 @name="binocle">],
  #  [4, #<Contact @id=14 @name="brioches">, #<Contact @id=40 @name="branchia">],
  #  [4, #<Contact @id=31 @name="bronchitic">, #<Contact @id=40 @name="branchia">]
  # ]
