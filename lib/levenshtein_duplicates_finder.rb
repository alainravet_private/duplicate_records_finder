#  In a Rails project, copy this in the lib directory
#  => it can be loaded with a simple require.
#    Ex:
#      require 'levenshtein_duplicates_finder'
#
#  Otherwise (if it is not on the LOADPATH), use require_relative
#    Ex:
#      require_relative '../mylib/levenshtein_duplicates_finder'

require 'levenshtein'

module LevenshteinDuplicatesFinder

  # Usage :
  #   class Contact
  #     extend LevenshteinDuplicatesFinder
  #   end
  #
  #   res = Contact.find_duplicates_with_levenshtein_distance_smaller_or_equal_to(:name, 4)
  #
  def find_duplicates_with_levenshtein_distance_smaller_or_equal_to(field, max_distance)
    model_class  = self
    permutations = []
    records = model_class.all
    records.each do |rec_a|
      field_value_a = rec_a.send(field)
      records.each do |rec_b|
         unless rec_b.id <= rec_a.id
           distance = Levenshtein.distance(field_value_a, rec_b.send(field))
           next unless distance <= max_distance
           permutations << [distance, rec_a, rec_b]
         end
      end
    end
    permutations
  end

  def self.included(base)
    raise "\n\n*** ERROR : the #{self} module must be extend-ed, not include-d\n\n"
  end

end
