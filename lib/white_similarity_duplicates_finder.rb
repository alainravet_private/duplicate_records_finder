#  In a Rails project, copy this in the lib directory
#  => it can be loaded with a simple require.
#    Ex:
#      require 'white_similarity_duplicates_finder'
#
#  Otherwise (if it is not on the LOADPATH), use require_relative
#    Ex:
#      require_relative '../mylib/white_similarity_duplicates_finder'

require 'text'
# see https://github.com/threedaymonk/text

module WhiteSimilarityDuplicatesFinder

  # Usage :
  #   class Contact
  #     extend WhiteSimilarityDuplicatesFinder
  #   end
  #
  #   res = Contact.find_duplicates_with_white_similarity_greater_or_equal_to(:name, 4)
  #
  def find_duplicates_with_white_similarity_greater_or_equal_to(field, min_similarity)
    model_class  = self
    permutations = []
    records = model_class.all
    white = Text::WhiteSimilarity.new

    records.each do |rec_a|
      field_value_a = rec_a.send(field)
      records.each do |rec_b|
         unless rec_b.id <= rec_a.id
           similarity = white.similarity(field_value_a, rec_b.send(field))
           next unless min_similarity <= similarity
           permutations << [similarity, rec_a, rec_b]
         end
      end
    end
    permutations
  end

  def self.included(base)
    raise "\n\n*** ERROR : the #{self} module must be extend-ed, not include-d\n\n"
  end

end
